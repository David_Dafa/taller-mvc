Estimado Profesor:
junto con saludarlo, le indicamos como ejecutar el taller mvc a mano 
de David Fuentes y Marcos Rojas.
1.- Ejecute el script "todolistoMarcosDavid.sql" en mysql., 
estan ya 4 usuarios creados, para que pruebe el programa.
2.- en la pantalla tareas se deja un lado para ingresar una nueva tarea y una tabla donde indica 
    las tareas creadas por ese usuario, tambien existen los botones mis tareas, este lo devolvera
    a la pantalla tareas, y el boton calendario, que lo redireccionara a calendario.
3.-  en calendario muestra las tareas creadas por el usuario, estas se crean por defecto el 15-04-18, 
    para que puedan ver que se muestran las tareas ya hechas, para ver si funciona correctamente.
4.- el link detalles muestra los detalles de la tarea, esta vista provee inmediatamente la funcion de 
editar la tarea.
5.- el link borrar tarea permite borrar una tarea del usuario x
6.- el administrador tiene permiso para poder ver las tareas de los usuarios, 
estas se muestran por usuario.
eso es todo Profesor
Saludos 