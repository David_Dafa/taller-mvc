<?php 

require("EstadoTarea.php");
require("TipoTarea.php");

class Tarea {
    private $id;
    private $titulo;
    private $descripcion;    
    private $estado;
    private $usuario;
    private $fecha;
    private $tipo;

    private static function fromRowToTarea($row) {
        return new Tarea($row);
    }
    public static function getAllTareas()
    {
        $query = "SELECT * FROM tarea ORDER BY usuario_id";
        $ps = Config::$dbh->prepare($query);
        $res = $ps->execute(array());
        $result = array();
        if ($res) {
            $result = $ps->fetchAll();
            $result = array_map([Tarea::class, 'fromRowToTarea'], $result);
        }

        return $result;
    }

        public static function getAllUserTareas($user) {
        $query = "SELECT * FROM tarea WHERE usuario_id = ?";
        $ps    = Config::$dbh->prepare($query);
        $user_id = $user->getId();
        $res   = $ps->execute(array($user_id));
        $result = array();
        if($res) {
            $result = $ps->fetchAll();
            $result = array_map([Tarea::class, 'fromRowToTarea'], $result);
        }

        return $result;        
    }


    public static function agregarTarea($titulo, $descripcion, $user_id, $estado_id, $tipo) {
        $query = "INSERT INTO tarea (titulo, descripcion, usuario_id, tipo_id, estado_id, fecha_inicio) VALUES (?, ?, ?, ?, ?, ?)";
        $ps    = Config::$dbh->prepare($query);
        $res   = $ps->execute(array(
                        $titulo,
                        $descripcion,
                        $user_id,
                        $tipo,                        
                        $estado_id,
                        "2018-04-27"
        ));
      
    }

    function __construct($result_row) {
        $this->id          = $result_row["tarea_id"];
        $this->titulo      = $result_row["titulo"];
        $this->descripcion = $result_row["descripcion"];        
        $this->estado      = $result_row["estado_id"];
        $this->usuario     = $result_row["usuario_id"];
        $this->fecha       = $result_row["fecha_inicio"];
        $this->tipo        = $result_row["tipo_id"]; //ver que pasa por que me tira este error.
    }

    public function getId() {
        return $this->id;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }
    
    public function getTipo() {
        //return $this->estado;
        return TipoTarea::getById($this->tipo);
    }
    public function getEstado() {
        //return $this->estado;
        return EstadoTarea::getById($this->estado);
    }
    public function getUser(){
        return Usuario::findById($this->usuario);
    }
    public function getFecha(){
        return $this->fecha;
    }
    public static function borrarTarea($id, $usuario) {
        
        $query = 'DELETE FROM tarea WHERE tarea_id= ? AND usuario_id= ?';
        $ps    = Config::$dbh->prepare($query);
        $res   = $ps->execute(array ($id, $usuario->getId()));
        return $res;
      
    }
     public static function actualizarTarea($id, $usuario, $descripcion, $titulo, $estado, $tipo) {
        //$_SESSION["message"].="se recogio la id". $id."\n";
        $query = 'UPDATE tarea SET titulo = ?, estado_id = ?, descripcion = ?, tipo_id =? WHERE tarea_id = ? AND usuario_id= ? ;';
        $ps    = Config::$dbh->prepare($query);
        $res   = $ps->execute(array ($titulo, $estado, $descripcion, $tipo, $id, $usuario->getId() ));

      return $res;
    }

    public static function actualizarTareaAdmin($id, $usuario, $descripcion, $titulo, $estado, $tipo) {
        //$_SESSION["message"].="se recogio la id". $id."\n";
        $query = 'UPDATE tarea SET titulo = ?, estado_id = ?, descripcion = ?, tipo_id =?, usuario_id= ?  WHERE tarea_id = ?;';
        $ps    = Config::$dbh->prepare($query);
        $res   = $ps->execute(array ($titulo, $estado, $descripcion, $tipo, $usuario , $id));

        return $res;
    }

    public static function getById($id) {
            $query = "SELECT * FROM tarea WHERE tarea_id = ?";
            $ps    = Config::$dbh->prepare($query);

        $res   = $ps->execute(array($id));
        $result = null;
        if($res) {
            $result = $ps->fetch();
            $result = Tarea::fromRowToTarea($result);
        }

        return $result;
    }
}

?>