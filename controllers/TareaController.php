<?php

require("models/Tarea.php");
require("views/Tareas.view.php");
require("views/TareasDetalles.view.php");
require("views/CalendarioTareas.view.php");
require("views/Administrador.view.php");
require("views/AdminTareasDetalles.view.php");

class TareaController {

    public function listadoTareas() {
        $user = $_SESSION["user"];        
        $tareas = Tarea::getAllUserTareas($user);        
        $estados = EstadoTarea::getAll();
        $tipo = TipoTarea::getAll();
        if($user->getRol()=="admin") {
            header('Location: ' . '/todolisto_mvc/mainController.php/administrador');

        }else{
                $tareasViews = new TareasView();
                echo $tareasViews->render($tareas, $estados, $tipo);
            }
        }
    
    public function agregarTarea($titulo, $desc, $estado_id, $tipo) {
        $user = $_SESSION["user"];
        Tarea::agregarTarea($titulo, $desc, $user->getId(), $estado_id, $tipo);        
        header('Location: ' . '/todolisto_mvc/mainController.php/tareas');
    }
    //Funcion para borrar tareas
    public function borrarTarea($id) {
        $user = $_SESSION["user"];
        Tarea::borrarTarea($id, $user);        
        header('Location: ' . '/todolisto_mvc/mainController.php/tareas');
    }
    public function actualizarTarea($id, $descripcion, $titulo, $estado, $tipo)
    {
        $user = $_SESSION["user"];
        $res = Tarea::actualizarTarea($id, $user, $descripcion, $titulo, $estado, $tipo);
        if ($res==true) {
            $_SESSION["message"].="<p>se realizo la operacion</p>";
            header('Location: ' . '/todolisto_mvc/mainController.php/tarea?id='.$id);


        } else {
            $_SESSION["message"].="<p>Fallo la operación</p>";
            header('Location: ' . '/todolisto_mvc/mainController.php/tarea?id='.$id);
        }
    }
    public function actualizarTareaAdmin($id, $descripcion, $titulo, $estado, $tipo, $user)
    {
        $res = Tarea::actualizarTareaAdmin($id, $user, $descripcion, $titulo, $estado, $tipo);
        if ($res==true) {
            $_SESSION["message"].="<p>se realizo la operacion</p>";
            header('Location: ' . '/todolisto_mvc/mainController.php/tarea?id='.$id);


        } else {
            $_SESSION["message"].="<p>Fallo la operación</p>";
            header('Location: ' . '/todolisto_mvc/mainController.php/tarea?id='.$id);
        }
    }
     
    
    public function calendario(){
        $user = $_SESSION["user"];        
        $tareas = Tarea::getAllUserTareas($user);        
        $estados = EstadoTarea::getAll();
        
        $tareasViews = new CalendarioTareasView();
        echo $tareasViews->render($tareas, $estados);
    }
    
    /**
     * @param $id
     */
    public function detalleTarea($id){
        $tarea=Tarea::getById($id);
        $estado=EstadoTarea::getAll();
        $tipo=TipoTarea::getAll();
        $users= Usuario::getAll();
        $tareasViews= new TareasDetallesView();
        if ($_SESSION["rol"]=="admin") {
            $tareasViews= new AdminTareasDetallesView();
            $tareasViews->render($tarea, $estado, $tipo, $users);
        }
        else {
            echo $tareasViews->render($tarea, $estado, $tipo);
        }
    }
    /**
        aca agrego la funcion de administrador, primero obtiene todas las tareas, luego los estados y por ultimo el
        tipo, luego llama a la vista de administrador y le pasa los parametros obtenidos.
    **/
    public function administrador(){
        $tareas=Tarea::getAllTareas();
        $estado=EstadoTarea::getAll();
        $tipo=TipoTarea::getAll();
        $administradorViews= new AdministradorView();
        echo $administradorViews->render($tareas, $estado, $tipo);

    }
    
}
?>