<?php

class TareasDetallesView {

    public function render($tarea, $estados, $tipos) { ?>
        <html xmlns="http://www.w3.org/1999/html">
            <head>
                <title>Todo Listo! / <?php echo $_SESSION["username"];?></title>
            </head>
            <body>
                <a href="/todolisto_mvc/mainController.php/logout">Cerrar Sesión</a>
                <?php echo $_SESSION["message"];$_SESSION["message"]="";?>
                <h1>Todo Listo!</h1>
                <h2><a href="/todolisto_mvc/mainController.php/tareas">Volver</a></h2>

                <h2>Mi tarea</h2>

                    <form  method="post" action="/todolisto_mvc/mainController.php/tarea?id="<?php echo $tarea->getId(); ?>>
                    <input name="notNull" value="<?php echo base64_encode($tarea->getId()); ?>" type="hidden" \>
                        <label for="id">Id</label>
                        <input name="id" value=" <?php echo $tarea->getId(); ?>" disabled />

                        <label for="titulo">Titulo</label>
                        <input name="titulo" value="<?php echo $tarea->getTitulo(); ?>"  />

                        <label for="descripcion">Descripcion</label>
                        <input name="descripcion" value="<?php echo $tarea->getDescripcion(); ?>"  />

                        <label for="estado">Estado</label>
                        <select name="estado">
                            <?php foreach ($estados as $estado): ?>
                            <option value="<?php echo $estado->getId(); ?>"
                                <?php if(($tarea->getEstado()->getId())==($estado->getId())): echo "selected"; ?>
                                    <?php endif; ?>
                            >
                                <?php echo $estado->getNombre();?>
                            </option>
                            <?php endforeach ?>
                        </select>
                        
                        <select name="tipo">
                            <?php foreach ($tipos as $tipo): ?>
                            <option value="<?php echo $tipo->getId(); ?>"
                                <?php if(($tarea->getTipo()->getId())==($tipo->getId())): echo "selected"; ?>
                                    <?php endif; ?>
                            >
                                <?php echo $tipo->getNombre();?>
                            </option>
                            <?php endforeach ?>
                        </select>
                        
                        <br/>
                        <br/>
                        <input type="submit" value="Actualizar tarea" />

                    </form>
                    </table>

            </body>
        </html>

    <?php }
}
?>