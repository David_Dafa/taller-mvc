<?php

class CalendarioTareasView {

    public function render($paramTareas, $estados) { ?>
        <html>
            <head>
                <title>Todo Listo! / <?php echo $_SESSION["username"];?></title>
                <script src="/todolisto_mvc/include/js/jquery.min.js"></script>
                <script src="/todolisto_mvc/include/js/moment.min.js"></script>
                <link rel="stylesheet" href="/todolisto_mvc/include/css/fullcalendar.min.css">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
                <script src="/todolisto_mvc/include/js/fullcalendar.min.js"></script>
                <script src="/todolisto_mvc/include/js/es.js"></script>
                
            </head>
            <body>   
                <div align= "right"><a href="/todolisto_mvc/mainController.php/logout">Cerrar Sesión</a></div>         
                <button><a href="/todolisto_mvc/mainController.php/tareas">Mis Tareas</a></button>
                <button><a href="/todolisto_mvc/mainController.php/calendario">Calendario</a></button>
                <div class="container">
                    <div class="col"></div>
                    <div class="col-7"><div id="calendario"></div></div>
                    <div class="col"></div>
                </div>
                
                    <script>
                        $(document).ready(function(){
                            $('#calendario').fullCalendar({
                                 events:[
                                    
                                     
                                    <?php foreach($paramTareas as $tarea) { ?>
                                    <?php echo "{\n" ?>
                                        id:<?php echo $tarea->getId(); ?>,
                                        title:'<?php echo $tarea->getTitulo(); ?>',
                                        start: '<?php echo $tarea->getFecha(); ?>',
                                        descripcion: "<?php echo $tarea->getDescripcion(); ?>"        
                                     <?php
                                         
                                         if (end($paramTareas)===$tarea)
                                         {
                                             echo "}";
                                         }
                                         else 
                                         {
                                             echo "},";
                                         }
                                     } ?>
                                     
                                 ]
                             });
                            
                        });
                    </script>
                    
                
            </body>
        </html>

    <?php }
}
?>