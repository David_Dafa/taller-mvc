<?php

class TareasView {

    public function render($paramTareas, $estados, $tipos) { ?>
        <html>
            <head>
                <title>Todo Listo! / <?php echo $_SESSION["username"];?></title>
            </head>
            <body>   
                <div align= "right"><a href="/todolisto_mvc/mainController.php/logout">Cerrar Sesión</a></div>         
                <?//php echo $_SESSION["message"]; $_SESSION["message"]="";?>
                <button><a href="/todolisto_mvc/mainController.php/tareas">Mis Tareas</a></button>
                <button><a href="/todolisto_mvc/mainController.php/calendario">Calendario</a></button>

                <table>
                <tr>
                <th>
                
                    <table>
                       <form method="POST" action="/todolisto_mvc/mainController.php/nuevaTarea">
                       <div class="form-group">
                            <input type="text" name="titulo" placeholder="Titulo" />
                       </div>
                            <input type="text" name="descripcion" placeholder="Descripcion" />      
                        <div class="form-group">
                           <select name="estado_id">
                            <option selected disabled>Estado Tarea</option>
                            <?php foreach($estados as $estado) { ?>
                                <option value="<?php echo $estado->getId(); ?>"><?php echo $estado->getNombre(); ?></option>
                            <?php } ?>
                            
                        </select>
                        </div>
                        <div class="form-group">
                           <select name="tipo_id">
                            <option selected disabled>Tipo Tarea</option>
                            <?php foreach($tipos as $tipo) { ?>
                                <option value="<?php echo $tipo->getId(); ?>"><?php echo $tipo->getNombre(); ?></option>
                            <?php } ?>
                            
                        </select>
                        </div>
  
                        <div class="form-group">
                        <input type="submit" value="Crear Tarea!" />
                        </div>
                    </form>
                    </table>
                </th>
                <th>
                <h2>Mis tareas</h2>

                    <table border =3>
                        <tr>
                            <th>Titulo</th>
                            <th>Descripcion</th>
                            <th>Estado</th>
                            <th colspan="2">opciones</th>
                        </tr>
                        <?php foreach($paramTareas as $tarea) { ?>
                        <tr>
                            <td>
                                <a href="<?php echo "/todolisto_mvc/mainController.php/tarea?id=" . $tarea->getId(); ?>">
                                    <?php echo $tarea->getTitulo(); ?>
                                </a>
                            </td>
                            <td><?php echo $tarea->getDescripcion(); ?></td>
                            <td><?php echo $tarea->getEstado()->getNombre(); ?></td>
                            <td>
                                <a href="<?php echo "/todolisto_mvc/mainController.php/borrarTarea?id=" . $tarea->getId(); ?>">
                                    Borrar
                                </a>
                            </td>
                            <td>
                                <a href="<?php echo "/todolisto_mvc/mainController.php/tarea?id=" . $tarea->getId(); ?>">
                                    Detalles
                                </a>
                            </td>
                            
                        </tr>
                        <?php } ?>
                    </table>
                    </th>
                    </tr>
                </table>
            </body>
        </html>

    <?php }
}
?>